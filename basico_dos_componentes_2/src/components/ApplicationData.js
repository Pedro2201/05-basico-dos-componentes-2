import React, { Component } from 'react';


class ApplicationData extends Component {

    constructor(props) {
        super(props)
        this.state = {

            userName: '',
            email: '',
            phoneNumber: '',
            description: '',

        }
    }

    showApplication = (value, changedInput) => {

        switch (changedInput) {
            case 'userName':
                this.setState({ userName: value })
                break;
            case 'email':
                this.setState({ email: value })
                break;
            case 'phoneNumber':
                this.setState({ phoneNumber: value })
                break;
            case 'description':
                this.setState({ description: value })
                break;
            default:

        }
    }

    validationEmail = () => {
        const email = this.state.email
        const regex = /^(([^<>()\\.,;:\s@"]+(\.[^<>()\\.,;:\s@"]+)*)|(".+"))@(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (regex.test(email)) {
            console.log('E-mail is valid')
            return true
        } else {
            console.error('E-mail is not invalid')
            return false
        }
    }

    validationPhone = () => {
        const phoneNumber = this.state.phoneNumber
        const regexPhoneNumber = /^[0-9]{4,5}[0-9]{4}$/

        if (regexPhoneNumber.test(phoneNumber)) {
            console.log('Phone number is valid')
            return true
        }
        else {
            console.error('Phone number is not valid')
        }
        return false
    }

    sendButton = event => {
        event.preventDefault();

        if (this.validationEmail() && this.validationPhone()) {
            const forms = {
                userName: this.state.userName,
                email: this.state.email,
                phoneNumber: this.state.phoneNumber,
                description: this.state.description
            }

            this.props.showApplication(forms)
            alert(`Obrigado ${this.state.userName}, seus dados foram enviados! `)

        } else {
            alert("Dados de Telefone e/ou Email incorretos ou faltando!")
        }
    }

    render() {
        return (
            <form className="inputsHome">
                <h1>Preencha o formulário Por Favor!</h1>
                <>
                    <ul>
                        <div>
                            <label>Nome: </label>
                            <input type="text" minLength="3" onChange={event => { this.showApplication(event.target.value, 'userName') }}
                                required="required" placeholder="Nome" />
                        </div> <br />

                        <div>
                            <label>E-mail: </label>
                            <input type="Email" onChange={event => { this.showApplication(event.target.value, 'email') }}
                                placeholder=".......@.....com.br" />
                        </div> <br />

                        <div>
                            <label>Numero de Telefone: </label>
                            <input type="TelNumber" maxLength="9" onChange={event => { this.showApplication(event.target.value, 'phoneNumber') }}
                                placeholder="123456789" />
                        </div> <br />

                        <div>
                            <label>Descrição: </label>
                            <textarea type="text" cols="40" rows="5" maxLength="300" onChange={event => { this.showApplication(event.target.value, 'description') }}
                                placeholder="Descrição" />
                        </div> <br />

                        <div >
                            <input className="button" type="submit" onClick={this.sendButton} />
                        </div>
                    </ul>
                </>
            </form>
        );
    }
}
export default ApplicationData;