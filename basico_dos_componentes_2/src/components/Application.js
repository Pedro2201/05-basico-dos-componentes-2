import React, { Component } from 'react';
import ApplicationData from './ApplicationData';
import Forms from './Forms';
import './StylesGerais.css';

class Application extends Component {

    state = {
        forms: {}
    }
    showApplication = (forms) => {
        this.setState({ forms })
    }

    render() {
        const { user } = this.props
        console.log(this)
        return (
            <div>
                <ApplicationData showApplication={this.showApplication} />
                <div className="dados">
                    <h1>Seus dados são:</h1>
                    <>
                        <Forms user={user}>
                        Nome: {this.state.forms.userName}<br />
                        E-mail: {this.state.forms.email}<br />
                        Nº de Telefone: {this.state.forms.phoneNumber}<br />
                        Descrição: {this.state.forms.description}<br />
                        </Forms>
                    </>
                </div>
            </div>

        );
    }
}
export default Application;